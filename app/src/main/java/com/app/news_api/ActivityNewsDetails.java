package com.app.news_api;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.news_api.model.Articles;
import com.squareup.picasso.Picasso;

public class ActivityNewsDetails extends AppCompatActivity {
    public static final String EXTRA_OBJC = "com.app.sample.news.EXTRA_OBJC";

    // give preparation animation activity transition
    public static void navigate(AppCompatActivity activity, View transitionImage, Articles obj) {
        Intent intent = new Intent(activity, ActivityNewsDetails.class);
        intent.putExtra(EXTRA_OBJC, obj);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionImage, EXTRA_OBJC);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

    private Toolbar toolbar;
    private ActionBar actionBar;
    // extra obj
    private Articles article;
    private View parent_view;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        parent_view = findViewById(android.R.id.content);

        // animation transition
        ViewCompat.setTransitionName(findViewById(R.id.image), EXTRA_OBJC);

        // get extra object
        article = (Articles) getIntent().getSerializableExtra(EXTRA_OBJC);
        initToolbar();
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fabToggle();

        ((TextView) findViewById(R.id.title)).setText(article.getTitle());
        ((TextView) findViewById(R.id.content)).setText(article.getDescription());
        ((TextView) findViewById(R.id.date)).setText(article.getPublishedAt());
        TextView author = (TextView) findViewById(R.id.author);
        author.setText("By "+article.getAuthor());
        author.setBackgroundColor(Color.parseColor("#039BE5"));

        if(article.getUrlToImage().equals("")){
            Picasso.with(this).load(R.drawable.news_header).into(((ImageView) findViewById(R.id.image)));
        }else{
            Picasso.with(this).load(article.getUrlToImage()).into(((ImageView) findViewById(R.id.image)));
        }
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("");
    }

    private void fabToggle() {
            fab.setImageResource(R.drawable.ic_nav_saved);
//            fab.setImageResource(R.drawable.ic_nav_saved_outline);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
            return true;
        }else{
            Snackbar.make(parent_view, item.getTitle() + " clicked", Snackbar.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_news_details, menu);
        return true;
    }

}
