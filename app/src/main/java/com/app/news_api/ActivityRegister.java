package com.app.news_api;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.news_api.data.BaseActivity;
import com.app.news_api.data.Tools;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

public class ActivityRegister extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "ActivityRegister";


    private Button btn_signup;
    private EditText mUsernameField;
    private EditText mPasswordField;

    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);



        btn_signup = (Button) findViewById(R.id.btn_signup);
        mUsernameField = (EditText) findViewById(R.id.input_username);
        mPasswordField = (EditText) findViewById(R.id.input_password);
        btn_signup.setOnClickListener(this);

        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]

        hideKeyboard();

        // for system bar in lollipop
        Tools.systemBarLolipop(this);
    }

    // [START on_start_check_user]
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }
    // [END on_start_check_user]

    private void createAccount(String username, String password) {
        if (!validateForm()) {
            return;
        }

        showProgressDialog();

        final String mName = username;

        // [START create_user]
        mAuth.createUserWithEmailAndPassword(username+"@news-api.com", password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
//                            Log.e(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setDisplayName(mName).build();

                            user.updateProfile(profileUpdates);

                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
//                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(ActivityRegister.this, "Registration failed!!!!",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // [START_EXCLUDE]
                        hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
        // [END create_user_with]
    }



    private void updateUI(FirebaseUser user) {
        hideProgressDialog();
        if (user != null) {
            //this means the user is logged in
            Toast.makeText(ActivityRegister.this, "Welcome "+ mUsernameField.getText().toString() +" .Discover best news around the world!!!",Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent(ActivityRegister.this,ActivityMain.class);
                    startActivity(i);
                    finish();
                }
            }, 2500);


        } else {
            //this means you are signed out
        }
    }


    private boolean validateForm() {
        boolean valid = true;

        String username = mUsernameField.getText().toString();
        if (TextUtils.isEmpty(username)) {
            mUsernameField.setError("Required.");
            valid = false;
        } else {
            if(username.length() < 4){
                mUsernameField.setError("4 or more characters...");
                valid = false;
            }else{
                mUsernameField.setError(null);
            }
        }

        String password = mPasswordField.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mPasswordField.setError("Required.");
            valid = false;
        } else {
            if(password.length() < 6){
                mPasswordField.setError("6 or more characters...");
                valid = false;
            }else{
                mPasswordField.setError(null);
            }
        }

        return valid;
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btn_signup:
                createAccount(mUsernameField.getText().toString(), mPasswordField.getText().toString());
                break;
            default:
                break;
        }


    }
}
