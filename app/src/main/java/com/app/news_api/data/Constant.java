package com.app.news_api.data;

import android.content.Context;

import com.app.news_api.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by muslim on 14/10/2015.
 */
public class Constant {

    public static List<String> getHomeCatgeory(Context ctx) {
        List<String> items = new ArrayList<>();
        String name_arr[] = ctx.getResources().getStringArray(R.array.home_category);
        for (int i = 0; i < name_arr.length; i++) {
            items.add(name_arr[i]);
        }
        return items;
    }

    //Data URL
    public static final String ARTICLES_URL = "https://newsapi.org/v1/articles";
    public static final String NEWS_HOME_URL = "https://newsapi.org/v1/articles?source=mirror";

    public static final String ALL_SOURCES_URL = "https://newsapi.org/v1/sources?language=en";

    public static final String TECH_SOURCES_URL = "https://newsapi.org/v1/sources?category=technology";

    //JSON TAGS
    public static final String NEWS_API_KEY = "5f41d95214204c33bb3802b43dee2109";
}
