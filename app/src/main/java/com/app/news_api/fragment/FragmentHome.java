package com.app.news_api.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.app.news_api.ActivityMain;
import com.app.news_api.ActivityNewsDetails;
import com.app.news_api.R;
import com.app.news_api.adapter.AdapterNewsListWithHeader;
import com.app.news_api.data.Constant;
import com.app.news_api.data.GlobalVariable;
import com.app.news_api.data.IResult;
import com.app.news_api.data.VolleyService;
import com.app.news_api.model.Articles;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FragmentHome extends Fragment {

    private String TAG = "FragmentHome";

    private View root_view;
    private GlobalVariable global;
    private TabLayout tabLayout;
    private RecyclerView recyclerView;

    private AdapterNewsListWithHeader mAdapter;

    // testing volley service
    IResult mResultCallback = null;
    VolleyService mVolleyService;

    //Creating a List of articles
    private List<Articles> listTopArticles;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.fragment_home, null);
        global = (GlobalVariable) getActivity().getApplication();

        recyclerView = (RecyclerView) root_view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        //Initializing our sources list
        listTopArticles = new ArrayList<>();

        // prepare tab layout
        initTabLayout();

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                // Init volley service
                getArticlesVolleyCallback(tab.getText().toString());

                callVolleyServiceGetData(tab.getText().toString());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) { }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        });

        return root_view;
    }

    private void displayListNews(List<Articles> items){

        if(items.isEmpty())
        {
            // Do something with the empty list here.
        }else{
            mAdapter = new AdapterNewsListWithHeader(getActivity(), items.get(items.size()-1), items);
            recyclerView.setAdapter(mAdapter);
            mAdapter.setOnItemClickListener(new AdapterNewsListWithHeader.OnItemClickListener() {
                @Override
                public void onItemClick(View v, Articles obj, int position) {
                    ActivityNewsDetails.navigate((ActivityMain)getActivity(), v.findViewById(R.id.image), obj);
                }
            });
        }
    }

    private void initTabLayout(){
        tabLayout = (TabLayout) root_view.findViewById(R.id.tabs);
        List<String> items_tab = Constant.getHomeCatgeory(getActivity());
        tabLayout.addTab(tabLayout.newTab().setText(items_tab.get(0)), true);

        // display first news
        // Init volley service
        getArticlesVolleyCallback(items_tab.get(0));

        callVolleyServiceGetData(items_tab.get(0));

        for (int i=1; i< items_tab.size(); i++){
            tabLayout.addTab(tabLayout.newTab().setText(items_tab.get(i)));
        }
    }

    private void callVolleyServiceGetData(String sortBy) {
        mVolleyService = new VolleyService(mResultCallback,getActivity());

        if(sortBy.equals("TOP")){
            mVolleyService.getDataStringRequest("GET",Constant.NEWS_HOME_URL+"&apiKey="+Constant.NEWS_API_KEY);
        }
        if(sortBy.equals("LATEST")){
            mVolleyService.getDataStringRequest("GET",Constant.NEWS_HOME_URL+"&sortBy=latest&apiKey="+Constant.NEWS_API_KEY);
        }

        if(sortBy.equals("TRENDING")){
            mVolleyService.getDataStringRequest("GET",Constant.NEWS_HOME_URL+"&sortBy=popular&apiKey="+Constant.NEWS_API_KEY);
        }

    }


    // testing volley service
    void getArticlesVolleyCallback(final String sortBy){

        mResultCallback = new IResult() {
            @Override
            public void stringRequestNotifySuccess(String requestType,String response) {
                String status = null;
                JSONObject jsonObjRes;

                try {
                    jsonObjRes = new JSONObject(response);
                    status = jsonObjRes.getString("status");
                    if(status.equals("ok")){
                        // source is json array
                        JSONArray articlesArray = jsonObjRes.getJSONArray("articles");
                        //Parse the json articles response
                        parseData(articlesArray);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType,VolleyError error) {

                if((error instanceof NetworkError) || (error instanceof NoConnectionError)){
                    Snackbar.make(root_view, "There is no internet connection!!!", Snackbar.LENGTH_SHORT).show();
                }else if((error instanceof ServerError)){
                    if(sortBy.equals("TOP")){
                        Snackbar.make(root_view, "There are no top articles!!!", Snackbar.LENGTH_SHORT).show();
                    }
                    if(sortBy.equals("LATEST")){
                        Snackbar.make(root_view, "There are no latest articles!!!", Snackbar.LENGTH_SHORT).show();
                    }

                    if(sortBy.equals("TRENDING")){
                        Snackbar.make(root_view, "There are no trending articles!!!", Snackbar.LENGTH_SHORT).show();
                    }
                }else{
                    Snackbar.make(root_view, error.toString(), Snackbar.LENGTH_SHORT).show();
                }

                if(!listTopArticles.isEmpty() ){
                    listTopArticles.clear();
                    mAdapter.notifyDataSetChanged();
                }else{

                }
            }
        };
    }

    //This method will parse json data
    private void parseData(JSONArray array) {

        for (int i = 0; i < array.length(); i++) {
            //Creating the object
            Articles article = new Articles();
            JSONObject json = null;
            try {
                //Getting json
                json = array.getJSONObject(i);
                //Adding data to the object
                article.setAuthor(json.getString("author"));
                article.setTitle(json.getString("title"));
                article.setDescription(json.getString("description"));
                article.setUrl(json.getString("url"));
                article.setUrlToImage(json.getString("urlToImage"));
                article.setPublishedAt(json.getString("publishedAt"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //Adding the object to the list
            listTopArticles.add(article);
        }

        //Notifying the adapter that data has been added or changed
        displayListNews(listTopArticles);

        mAdapter.notifyDataSetChanged();
    }


}
